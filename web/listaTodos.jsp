<%@ page import="br.carros.dao.CarrosDao" %>
<%@ page import="br.carros.entidade.Carro" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: mayane.de.mesquita
  Date: 24/05/2018
  Time: 09:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<link rel="stylesheet" type="text/css" href="boot/css/bootstrap.css">
<head>
    <title>Title</title>
</head>
<body>
<hr>
<% CarrosDao carrosDao = new CarrosDao();
    ArrayList<Carro> carros = carrosDao.getTodos();
%>

<table>
    <tr>
        <td>Fabricante</td>
        <td>Modelo</td>
        <td>Placa</td>
    </tr>
   <tr>
       <% for (Carro c : carros) {
           out.print("<tr>");
           out.print("<td>" + c.getFabricante() + "<td>");
           out.print("<td>" + c.getModelo() + "<td>");
           out.print("<td>" + c.getPlaca() + "<td>");
           out.print("<td>" + "<a href = 'excluirCarro.jsp? idCarro =" + c.getIdCarro() + "'>Excluir Carro</a> " + "</td>");
           out.print("<td>" + "<a href = 'editarCarro.jsp? idCarro =" + c.getIdCarro() + "'>Editar Carro</a> " + "</td>");
           out.print("<tr>");
       }
    %>
   </tr>
    </table>
 <a href="incluirCarro.jsp">Cadastro Carro</a>

</body>
</html>
