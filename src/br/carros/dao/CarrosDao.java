package br.carros.dao;

import br.carros.entidade.Carro;

import java.sql.*;
import java.util.ArrayList;

public class CarrosDao {
 private static final String urlDB = "jdbc:mysql://localhost:3306/carros_db";
 private static Connection conn = null;
 private static Statement statement = null;

    public ArrayList<Carro> getTodos() throws SQLException, ClassNotFoundException {
            ArrayList<Carro> carros = new ArrayList<>();
            Connection conexao = ConexaoCarro.getConnection();
            //conn = DriverManager.getConnection(urlDB, "root", "root");
            // statement = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String select = "SELECT * FROM TB_CARROS";
            PreparedStatement statement2 = conexao.prepareStatement(select);
            ResultSet resultSet = statement2.executeQuery(select);

            while (resultSet.next()) {
                Carro carro = new Carro();
                carro.setIdCarro(Integer.parseInt(resultSet.getString("id")));
                carro.setFabricante(resultSet.getString("fabricante"));
                carro.setModelo(resultSet.getString("modelo"));
                carro.setPlaca(resultSet.getString("placa"));
                carro.setAno(resultSet.getInt("ano"));
                carro.setValor(resultSet.getFloat("valor"));
                carros.add(carro);
            }
           conexao.close();
            return carros;
    }




    public Carro getCarro (int idCarro) throws SQLException, ClassNotFoundException {
        Carro carro = null;
        Connection conexao = ConexaoCarro.getConnection();
        //conn = DriverManager.getConnection(urlDB, "root", "root");
        //statement = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        String select = "SELECT * FROM TB_CARROS WHERE ID = ?";
        PreparedStatement statement1 = conexao.prepareStatement(select);
        statement1.setInt(1, idCarro);
       // statement.setInt(1, idCarro);
        ResultSet result = statement.executeQuery(select);
        if (result.next()){
            carro.setIdCarro(result.getInt("id"));
            carro.setFabricante(result.getString("fabricante"));
            carro.setModelo(result.getString("modelo"));
        }
        conexao.close();
        return carro;
    }
}
